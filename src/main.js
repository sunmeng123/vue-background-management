import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'element-plus/lib/theme-chalk/index.css'

const app = createApp(App)

app.use(store).use(router).mount('#app')

// 1.我们先自己构造一些假数据  能实现根据位置内容渲染
