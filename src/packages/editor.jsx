import { computed, defineComponent, inject } from 'vue'
import EditorBlock from './editor-blocks'
import style from './editor.module.scss'

export default defineComponent({
  props: {
    modelValue: {
      type: Object
    }
  },

  components: {
    EditorBlock
  },
  setup(props) {
    console.log(props, 'props')
    const data = computed({
      get() {
        return props.modelValue
      }
    })
    // 画布样式
    const containerStyles = computed({
      get() {
        return {
          width: data.value.container.width + 'px',
          height: data.value.container.height + 'px'
        }
      }
    })
    const config = inject('config')

    return () => (
      <div className={style.editor}>
        <div className={style['editor-left']}>
          左侧物料区
          {config.componentList.map((component) => {
            console.log(
              config.componentList,
              style,
              style['editor-left-item'],
              'config11'
            )
            return (
              <div className={style['editor-left-item']}>
                <span>{component.label}</span>
                <div>{component.preview()}</div>
              </div>
            )
          })}
        </div>
        <div className={style['editor-top']}>菜单栏</div>
        <div className={style['editor-right']}>属性控制栏目</div>
        <div className={style['editor-container']}>
          {/* 负责生产滚动条 */}
          <div className={style['editor-container-canvas']}>
            {/* 生产内容区域 */}
            <div
              className={style['editor-container-canvas-content']}
              style={containerStyles.value}
            >
              内容区
              {data.value.blocks.map((bloack) => {
                return <EditorBlock bloack={bloack}></EditorBlock>
              })}
            </div>
          </div>
        </div>
      </div>
    )
  }
})
