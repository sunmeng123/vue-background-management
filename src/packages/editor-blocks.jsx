import { defineComponent, computed, inject } from 'vue'
import style from './editor.module.scss'
export default defineComponent({
  props: {
    bloack: {
      type: Object
    }
  },

  setup(props) {
    const bloackStyles = computed(() => {
      return {
        top: props.bloack.top + 'px',
        left: props.bloack.top + 'px',
        zIndex: props.bloack.zIndex
      }
    })
    const config = inject('config')
    return () => {
      const component = config.componentMap[props.bloack.key]
      // 获取渲染render
      const RenderComponent = component.render()
      console.log(RenderComponent, 'RenderComponent')
      return (
        <div className={style['editor-bloack']} style={bloackStyles.value}>
          {RenderComponent}
        </div>
      )
    }
  }
})
