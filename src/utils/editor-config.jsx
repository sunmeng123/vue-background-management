// 列表区域显示所有物料
// key 对应的组件映射关系
import { ElButton, ElInput } from 'element-plus'
function creactEditorConfig() {
  const componentList = []
  const componentMap = {}

  return {
    componentList,
    componentMap,
    register: (component) => {
      componentList.push(component)
      componentMap[component.key] = component
    }
  }
}

export const registerConfig = creactEditorConfig()

registerConfig.register({
  label: '文本',
  preview: () => {
    console.log('预览文本')
    return '预览文本'
  },
  render: () => {
    console.log('渲染文本')
    return '渲染文本'
  },
  key: 'text'
})

registerConfig.register({
  label: '按钮',
  preview: () => {
    console.log('预览按钮')
    return <ElButton>预览按钮</ElButton>
  },
  render: () => {
    console.log('渲染按钮')
    return <ElButton>预览按钮</ElButton>
  },
  key: 'button'
})

registerConfig.register({
  label: '输入框',
  preview: () => {
    console.log('预览输入框')
    return <ElInput placeholder="预览输入框">预览按钮</ElInput>
  },
  render: () => {
    console.log('渲染输入框')
    return <ElInput placeholder="渲染输入框">预览按钮</ElInput>
  },
  key: 'input'
})
